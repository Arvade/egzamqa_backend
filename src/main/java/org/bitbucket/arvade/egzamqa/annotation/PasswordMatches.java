package org.bitbucket.arvade.egzamqa.annotation;

import org.bitbucket.arvade.egzamqa.validator.impl.PasswordMatchesValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Target({TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PasswordMatchesValidator.class)
public @interface PasswordMatches {

    String message() default "Passwords has to match";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
