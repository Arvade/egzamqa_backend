package org.bitbucket.arvade.egzamqa.repository;

import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.model.UserSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface UserSessionRepository extends JpaRepository<UserSession, Long> {

    UserSession findById(Integer id);

    UserSession findTop1BySecretAndUserAndExpirationDateGreaterThan(String secret, User user, Date date);

    UserSession findTop1ByIdOrderByExpirationDateDesc(Long id);

    UserSession findTop1ByExpirationDateBeforeAndUserOrderByExpirationDateDesc(Date date, User user);

    UserSession findTop1ByUserOrderByExpirationDateDesc(User user);
}
