package org.bitbucket.arvade.egzamqa.repository;

import org.bitbucket.arvade.egzamqa.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

    User findByEmail(String email);

    User findById(Long id);
}
