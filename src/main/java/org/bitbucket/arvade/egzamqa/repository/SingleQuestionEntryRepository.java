package org.bitbucket.arvade.egzamqa.repository;

import org.bitbucket.arvade.egzamqa.model.Question;
import org.bitbucket.arvade.egzamqa.model.SingleQuestionEntry;
import org.bitbucket.arvade.egzamqa.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SingleQuestionEntryRepository extends JpaRepository<SingleQuestionEntry, Long> {

    SingleQuestionEntry findTop1ByUserAndQuestionOrderByRequestDateDesc(User user, Question question);
}
