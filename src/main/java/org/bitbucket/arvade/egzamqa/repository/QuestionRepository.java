package org.bitbucket.arvade.egzamqa.repository;

import org.bitbucket.arvade.egzamqa.model.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

    @Query(value = "SELECT * FROM questions WHERE id NOT IN :idList ORDER BY RAND() LIMIT 1", nativeQuery = true)
    Question findRandomExceptIds(@Param("idList") List<Long> idList);

    @Query(value = "SELECT * FROM questions ORDER BY RAND() LIMIT 1", nativeQuery = true)
    Question findRandom();
}