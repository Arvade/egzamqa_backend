package org.bitbucket.arvade.egzamqa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(value = "org.bitbucket.arvade.egzamqa.model")
public class Egzamqa {

    public static void main(String[] args) {
        SpringApplication.run(Egzamqa.class, args);
    }
}
