package org.bitbucket.arvade.egzamqa.service.impl;

import org.bitbucket.arvade.egzamqa.factory.SingleQuestionEntryFactory;
import org.bitbucket.arvade.egzamqa.model.Question;
import org.bitbucket.arvade.egzamqa.model.SingleQuestionEntry;
import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.repository.SingleQuestionEntryRepository;
import org.bitbucket.arvade.egzamqa.service.SingleQuestionEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SingleQuestionEntryServiceImpl implements SingleQuestionEntryService {

    private SingleQuestionEntryRepository singleQuestionEntryRepository;
    private SingleQuestionEntryFactory singleQuestionEntryFactory;

    @Autowired
    public SingleQuestionEntryServiceImpl(SingleQuestionEntryRepository singleQuestionEntryRepository,
                                          SingleQuestionEntryFactory singleQuestionEntryFactory) {
        this.singleQuestionEntryRepository = singleQuestionEntryRepository;
        this.singleQuestionEntryFactory = singleQuestionEntryFactory;
    }

    @Override
    public void registerSingleQuestionDownload(Question question, User user) {
        SingleQuestionEntry entry = singleQuestionEntryFactory.createSingleQuestionEntry(question, user);
        singleQuestionEntryRepository.save(entry);
    }

    @Override
    public void answerOnDownloadedQuestion(Long questionId, User user, boolean isCorrect) {
        Question question = new Question();
        question.setId(questionId);
        SingleQuestionEntry singleQuestionEntry = singleQuestionEntryRepository
                .findTop1ByUserAndQuestionOrderByRequestDateDesc(user, question);
        singleQuestionEntry.setResponseDate(new Date());
        singleQuestionEntry.setAnswerResult(isCorrect);
        singleQuestionEntryRepository.save(singleQuestionEntry);
    }
}
