package org.bitbucket.arvade.egzamqa.service;

import org.bitbucket.arvade.egzamqa.model.User;

public interface PermissionService {

    boolean hasPermission(String actionName, User user);
}