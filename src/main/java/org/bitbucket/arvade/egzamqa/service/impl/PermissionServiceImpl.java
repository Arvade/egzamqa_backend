package org.bitbucket.arvade.egzamqa.service.impl;

import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.provider.PermissionsProvider;
import org.bitbucket.arvade.egzamqa.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class PermissionServiceImpl implements PermissionService {

    private PermissionsProvider permissionsProvider;

    @Autowired
    public PermissionServiceImpl(PermissionsProvider permissionsProvider) {
        this.permissionsProvider = permissionsProvider;
    }

    @Override
    public boolean hasPermission(String actionName, User user) {
        Map<String, HashMap<String, Boolean>> permissions = permissionsProvider.getPermissions();
        String userRole = user.getRole();
        HashMap<String, Boolean> permissionsForUserRole = permissions.get(userRole);

        if (!permissionsForUserRole.containsKey(actionName)) {
            throw new IllegalArgumentException("Permission for '" + actionName + "' doesn't exists.");
        }

        return permissionsForUserRole.get(actionName);
    }
}