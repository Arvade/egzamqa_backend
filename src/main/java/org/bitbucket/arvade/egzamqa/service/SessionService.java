package org.bitbucket.arvade.egzamqa.service;

import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.model.UserSession;
import org.bitbucket.arvade.egzamqa.resource.ResponseResource;

import java.util.Optional;

public interface SessionService {

    void save(UserSession userSession);

    Optional<UserSession> findTheMostRecentUserSessionById(Long id);

    Boolean hasActiveSession(User user);

    Boolean isActive(String token);

    String getAuthToken(UserSession userSession);

    String[] decodeAuthTokenToTokenParts(String token);

    ResponseResource checkSession(String authToken);
}
