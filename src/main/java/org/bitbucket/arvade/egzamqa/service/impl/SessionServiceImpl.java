package org.bitbucket.arvade.egzamqa.service.impl;

import lombok.Setter;
import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.model.UserSession;
import org.bitbucket.arvade.egzamqa.repository.UserSessionRepository;
import org.bitbucket.arvade.egzamqa.resource.ResponseResource;
import org.bitbucket.arvade.egzamqa.service.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.Date;
import java.util.Optional;

@Service
public class SessionServiceImpl implements SessionService {

    private static final String TOKEN_DEFAULT_TEMPLATE = "%d:%s";

    @Setter
    private String tokenDefaultTemplate = TOKEN_DEFAULT_TEMPLATE;

    private UserSessionRepository userSessionRepository;
    private Base64.Encoder encoder;
    private Base64.Decoder decoder;

    @Autowired
    public SessionServiceImpl(UserSessionRepository userSessionRepository,
                              Base64.Encoder encoder,
                              Base64.Decoder decoder) {
        this.userSessionRepository = userSessionRepository;
        this.encoder = encoder;
        this.decoder = decoder;
    }

    @Override
    public void save(UserSession userSession) {
        this.userSessionRepository.save(userSession);
    }

    @Override
    public Optional<UserSession> findTheMostRecentUserSessionById(Long id) {
        return Optional.ofNullable(userSessionRepository.findTop1ByIdOrderByExpirationDateDesc(id));
    }

    @Override
    public Boolean hasActiveSession(User user) {
        return Optional.ofNullable(userSessionRepository
                .findTop1ByExpirationDateBeforeAndUserOrderByExpirationDateDesc(new Date(), user))
                .isPresent();
    }

    @Override
    public Boolean isActive(String token) {
        String[] parts = decodeAuthTokenToTokenParts(token);
        Long userId = Long.parseLong(parts[0]);
        String secret = parts[1];


        Optional<UserSession> userSessionOptional = Optional
                .ofNullable(userSessionRepository
                        .findTop1BySecretAndUserAndExpirationDateGreaterThan(secret, new User(userId), new Date()));

        return userSessionOptional.isPresent();
    }

    @Override
    public String[] decodeAuthTokenToTokenParts(String authToken) {
        byte[] decode = decoder.decode(authToken);
        String decodedToken = new String(decode);
        return decodedToken.split(":");
    }

    @Override
    public ResponseResource checkSession(String authToken) {
        ResponseResource response = new ResponseResource();

        if (!isActive(authToken)) {
            response.setHttpStatus(HttpStatus.UNAUTHORIZED);
            response.addError("Your session expired. Please log in");
        }

        return response;
    }

    @Override
    public String getAuthToken(UserSession userSession) {
        Long userId = userSession.getUser().getId();
        String secret = userSession.getSecret();

        String rawToken = String.format(tokenDefaultTemplate, userId, secret);
        byte[] encodedToken = this.encoder.encode(rawToken.getBytes());
        return new String(encodedToken);
    }
}