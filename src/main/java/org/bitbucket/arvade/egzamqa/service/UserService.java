package org.bitbucket.arvade.egzamqa.service;

import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.resource.LoginResultResource;
import org.bitbucket.arvade.egzamqa.resource.UserLoginRequestResource;
import org.bitbucket.arvade.egzamqa.resource.UserRegistrationResource;

import java.util.List;
import java.util.Optional;

public interface UserService {

    void save(User user);

    Optional<User> findById(Long id);

    Optional<User> findByUsername(String username);

    List<String> register(UserRegistrationResource userRegistrationResource);

    LoginResultResource login(UserLoginRequestResource userLoginRequestResource);

    User getUserForAuthToken(String authToken);
}
