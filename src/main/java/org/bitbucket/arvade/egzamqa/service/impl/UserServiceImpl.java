package org.bitbucket.arvade.egzamqa.service.impl;

import lombok.Setter;
import org.bitbucket.arvade.egzamqa.factory.UserSessionFactory;
import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.model.UserSession;
import org.bitbucket.arvade.egzamqa.repository.UserRepository;
import org.bitbucket.arvade.egzamqa.resource.LoginResultResource;
import org.bitbucket.arvade.egzamqa.resource.UserLoginRequestResource;
import org.bitbucket.arvade.egzamqa.resource.UserRegistrationResource;
import org.bitbucket.arvade.egzamqa.service.UserService;
import org.bitbucket.arvade.egzamqa.service.SessionService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private static final String DEFAULT_LOGIN_ERROR_MESSAGE = "Invalid username and password";

    @Setter
    private String defaultLoginErrorMessage = DEFAULT_LOGIN_ERROR_MESSAGE;

    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private UserSessionFactory userSessionFactory;
    private UserRepository userRepository;
    private ModelMapper modelMapper;
    private SessionService sessionService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           BCryptPasswordEncoder bCryptPasswordEncoder,
                           UserSessionFactory userSessionFactory,
                           ModelMapper modelMapper,
                           SessionService sessionService) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userSessionFactory = userSessionFactory;
        this.modelMapper = modelMapper;
        this.sessionService = sessionService;
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public Optional<User> findById(Long id) {
        return Optional.ofNullable(userRepository.findById(id));
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return Optional.ofNullable(userRepository.findByUsername(username));
    }

    @Override
    public List<String> register(UserRegistrationResource userRegistrationResource) {
        List<String> errors = new ArrayList<>();

        if (checkIfUsernameAlreadyTaken(userRegistrationResource.getUsername())) {
            errors.add("Username already taken");
        }

        if (checkIfEmailAlreadyTaken(userRegistrationResource.getEmail())) {
            errors.add("Email already taken");
        }

        if (errors.isEmpty()) {
            User user = modelMapper.map(userRegistrationResource, User.class);
            String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
            user.setPassword(encodedPassword);
            user.setRole("user");
            userRepository.save(user);
        }

        return errors;
    }

    @Override
    public LoginResultResource login(UserLoginRequestResource userLoginRequestResource) {
        LoginResultResource result = new LoginResultResource();

        if (checkIfNullProvided(userLoginRequestResource)) {
            result.addError(defaultLoginErrorMessage);
            result.setResult(false);
            return result;
        }

        String username = userLoginRequestResource.getUsername();
        Optional<User> userOptional = Optional.ofNullable(userRepository.findByUsername(username));

        if (!userOptional.isPresent()) {
            result.addError(defaultLoginErrorMessage);
            result.setResult(false);
            return result;
        }

        User user = userOptional.get();
        String encodedPassword = user.getPassword();
        String rawPassword = userLoginRequestResource.getPassword();

        boolean passwordMatch = bCryptPasswordEncoder.matches(rawPassword, encodedPassword);
        if (!passwordMatch) {
            result.addError(defaultLoginErrorMessage);
            result.setResult(false);
            return result;
        }

        UserSession userSession = userSessionFactory.createSession(user);
        sessionService.save(userSession);

        String authToken = sessionService.getAuthToken(userSession);
        result.setToken(authToken);

        return result;
    }

    @Override
    public User getUserForAuthToken(String authToken) {
        String[] authTokenParts = sessionService.decodeAuthTokenToTokenParts(authToken);
        Long userId = Long.parseLong(authTokenParts[0]);
        return findById(userId)
                .orElseThrow(() -> new IllegalArgumentException("User with id " + userId + " doesn't exists"));
    }

    private boolean checkIfUsernameAlreadyTaken(String username) {
        return Optional.ofNullable(userRepository.findByUsername(username)).isPresent();
    }

    private boolean checkIfEmailAlreadyTaken(String email) {
        return Optional.ofNullable(userRepository.findByEmail(email)).isPresent();
    }

    private boolean checkIfNullProvided(UserLoginRequestResource userLoginRequestResource) {
        return userLoginRequestResource.getUsername() == null || userLoginRequestResource.getPassword() == null;
    }
}
