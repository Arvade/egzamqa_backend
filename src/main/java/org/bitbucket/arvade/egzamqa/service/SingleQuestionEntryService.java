package org.bitbucket.arvade.egzamqa.service;

import org.bitbucket.arvade.egzamqa.model.Question;
import org.bitbucket.arvade.egzamqa.model.User;

public interface SingleQuestionEntryService {

    void registerSingleQuestionDownload(Question question, User user);

    void answerOnDownloadedQuestion(Long questionId, User user, boolean isCorrect);

}
