package org.bitbucket.arvade.egzamqa.service;

import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.resource.QuestionResource;
import org.bitbucket.arvade.egzamqa.resource.SingleQuestionResource;

import java.util.List;

public interface QuestionService {

    List<QuestionResource> getAll();

    SingleQuestionResource getSingleRandomQuestion(User user);

    SingleQuestionResource getSingleQuestionExcept(User user, List<Long> questionsIds);

    Integer getCorrectAnswerIndex(Long questionId);
}
