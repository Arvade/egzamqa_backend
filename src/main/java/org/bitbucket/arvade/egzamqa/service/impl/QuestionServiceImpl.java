package org.bitbucket.arvade.egzamqa.service.impl;

import org.bitbucket.arvade.egzamqa.model.Answer;
import org.bitbucket.arvade.egzamqa.model.Question;
import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.repository.QuestionRepository;
import org.bitbucket.arvade.egzamqa.resource.QuestionResource;
import org.bitbucket.arvade.egzamqa.resource.SingleQuestionResource;
import org.bitbucket.arvade.egzamqa.service.QuestionService;
import org.bitbucket.arvade.egzamqa.service.SingleQuestionEntryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class QuestionServiceImpl implements QuestionService {


    private SingleQuestionEntryService singleQuestionEntryService;
    private QuestionRepository questionRepository;
    private ModelMapper modelMapper;

    @Autowired
    public QuestionServiceImpl(SingleQuestionEntryService singleQuestionEntryService,
                               QuestionRepository questionRepository,
                               ModelMapper modelMapper) {
        this.singleQuestionEntryService = singleQuestionEntryService;
        this.questionRepository = questionRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<QuestionResource> getAll() {
        return questionRepository.findAll().stream()
                .map(question -> modelMapper.map(question, QuestionResource.class))
                .collect(Collectors.toList());
    }

    @Override
    public SingleQuestionResource getSingleRandomQuestion(User user) {
        Optional<Question> randomQuestionOptional = Optional.ofNullable(questionRepository.findRandom());
        if (!randomQuestionOptional.isPresent()) {
            throw new IllegalStateException(("No random question found"));
        }

        Question question = randomQuestionOptional.get();
        SingleQuestionResource singleQuestionResource = modelMapper.map(question, SingleQuestionResource.class);
        singleQuestionEntryService.registerSingleQuestionDownload(question, user);

        return singleQuestionResource;
    }

    @Override
    public SingleQuestionResource getSingleQuestionExcept(User user, List<Long> questionsIds) {
        //TODO: Implement
        return new SingleQuestionResource();
    }

    @Override
    public Integer getCorrectAnswerIndex(Long questionId) {
        Optional<Question> questionOptional = Optional.ofNullable(questionRepository.findOne(questionId));

        if (!questionOptional.isPresent()) {
            throw new IllegalArgumentException("Question with id " + questionId + " doesn't exists");
        }

        Question question = questionOptional.get();
        Integer correctAnswerIndex = getCorrectAnswerIndex(question.getAnswers());

        if (correctAnswerIndex == -1) {
            throw new IllegalStateException("No correct answer found.");
        }

        return correctAnswerIndex;
    }

    private Integer getCorrectAnswerIndex(List<Answer> answerList) {
        for (int i = 0; i < answerList.size(); i++) {
            Answer answer = answerList.get(i);
            if (answer.getIsCorrect()) {
                return i;
            }
        }
        return -1;
    }
}
