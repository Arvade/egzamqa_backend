package org.bitbucket.arvade.egzamqa.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserSession {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String secret;

    @ManyToOne
    private User user;

    @Column(nullable = false)
    private Date expirationDate;

    public UserSession(String secret, User user, Date expirationDate) {
        this.secret = secret;
        this.user = user;
        this.expirationDate = expirationDate;
    }
}
