package org.bitbucket.arvade.egzamqa.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class SingleQuestionEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private User user;

    @OneToOne
    private Question question;

    @Column
    private Boolean answerResult;

    @Column
    private Date requestDate;

    @Column
    private Date responseDate;
}
