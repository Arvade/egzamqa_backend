package org.bitbucket.arvade.egzamqa.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private Date creationDate;

    @Column(nullable = false)
    private String role;

    @PrePersist
    protected void onCreate() {
        creationDate = new Date();
    }

    public User(Long id){
        this.id = id;
    }

    public User(String username, String password, String email, Date creationDate, String role) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.creationDate = creationDate;
        this.role = role;
    }
}