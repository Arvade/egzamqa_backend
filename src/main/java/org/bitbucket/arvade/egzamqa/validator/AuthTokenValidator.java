package org.bitbucket.arvade.egzamqa.validator;

public interface AuthTokenValidator {

    Boolean validate(String authToken);
}
