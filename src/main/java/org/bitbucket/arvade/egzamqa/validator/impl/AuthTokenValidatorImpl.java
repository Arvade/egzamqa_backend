package org.bitbucket.arvade.egzamqa.validator.impl;

import org.bitbucket.arvade.egzamqa.validator.AuthTokenValidator;
import org.springframework.stereotype.Component;

@Component
public class AuthTokenValidatorImpl implements AuthTokenValidator {

    @Override
    public Boolean validate(String authToken) {
        return checkIfAuthTokenIsCorrect(authToken);
    }

    private boolean checkIfAuthTokenIsCorrect(String authToken){
        return authToken != null && !authToken.isEmpty() && !authToken.equals("null");
    }
}
