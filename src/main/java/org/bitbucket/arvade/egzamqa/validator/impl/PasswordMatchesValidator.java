package org.bitbucket.arvade.egzamqa.validator.impl;

import org.bitbucket.arvade.egzamqa.annotation.PasswordMatches;
import org.bitbucket.arvade.egzamqa.resource.UserRegistrationResource;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, UserRegistrationResource> {


    @Override
    public void initialize(PasswordMatches passwordMatches) {
        // Don't need to
    }

    @Override
    public boolean isValid(UserRegistrationResource userRegistrationResource, ConstraintValidatorContext context) {
        String password = userRegistrationResource.getPassword();
        String passwordConfirm = userRegistrationResource.getPasswordConfirm();
        if(password == null || passwordConfirm == null){
            return false;
        }
        return password.equals(passwordConfirm);
    }
}
