package org.bitbucket.arvade.egzamqa.provider;

import org.bitbucket.arvade.egzamqa.config.UserSessionConfig;

public interface UserSessionConfigProvider {

    UserSessionConfig getDefault();
}
