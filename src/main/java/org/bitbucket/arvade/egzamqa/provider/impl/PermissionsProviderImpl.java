package org.bitbucket.arvade.egzamqa.provider.impl;

import lombok.Setter;
import org.bitbucket.arvade.egzamqa.provider.PermissionsProvider;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@Component
public class PermissionsProviderImpl implements PermissionsProvider {

    private static final String DEFAULT_PERMISSION_FILE_NAME = "permissions.json";

    @Setter
    private String defaultPermissionFileName = DEFAULT_PERMISSION_FILE_NAME;

    private JSONParser jsonParser;

    private HashMap<String, HashMap<String, Boolean>> permissions;

    @Autowired
    public PermissionsProviderImpl(JSONParser jsonParser) {
        this.jsonParser = jsonParser;
    }


    @Override
    public Map<String, HashMap<String, Boolean>> getPermissions() {
        if (permissions != null) {
            return permissions;
        }

        URL url = jsonParser.getClass().getClassLoader().getResource(defaultPermissionFileName);


        try {
            if (url == null) {
                throw new IllegalStateException("permissions.json file not found");
            }

            Object parsedJsonFile = jsonParser.parse(new FileReader(new File(url.toURI())));
            permissions = (HashMap<String, HashMap<String, Boolean>>) parsedJsonFile;
        } catch (URISyntaxException | ParseException | IOException e) {
            throw new RuntimeException(e);
        }

        return permissions;
    }
}
