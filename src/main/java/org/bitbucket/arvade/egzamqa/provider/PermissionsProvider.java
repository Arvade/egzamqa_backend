package org.bitbucket.arvade.egzamqa.provider;

import java.util.HashMap;
import java.util.Map;

public interface PermissionsProvider {

    Map<String, HashMap<String, Boolean>> getPermissions();
}
