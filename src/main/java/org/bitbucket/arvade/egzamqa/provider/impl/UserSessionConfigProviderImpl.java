package org.bitbucket.arvade.egzamqa.provider.impl;

import org.bitbucket.arvade.egzamqa.config.UserSessionConfig;
import org.bitbucket.arvade.egzamqa.provider.UserSessionConfigProvider;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
public class UserSessionConfigProviderImpl implements UserSessionConfigProvider {

    private UserSessionConfig userSessionConfig = null;

    @Override
    public UserSessionConfig getDefault() {
        if (userSessionConfig == null) {
            createDefaultConfig();
        }

        return this.userSessionConfig;
    }

    private void createDefaultConfig() {
        this.userSessionConfig = UserSessionConfig.builder()
                .sessionDuration(Duration.ofHours(2))
                .build();
    }
}
