package org.bitbucket.arvade.egzamqa.controller;

import org.bitbucket.arvade.egzamqa.resource.LoginResultResource;
import org.bitbucket.arvade.egzamqa.resource.ResponseResource;
import org.bitbucket.arvade.egzamqa.resource.UserLoginRequestResource;
import org.bitbucket.arvade.egzamqa.resource.UserRegistrationResource;
import org.bitbucket.arvade.egzamqa.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@RestController
public class AuthController {

    private UserService userService;
    private Validator validator;

    @Autowired
    public AuthController(UserService userService, Validator validator) {
        this.userService = userService;
        this.validator = validator;
    }

    @CrossOrigin()
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseResource register(@RequestBody UserRegistrationResource userRegistrationResource) {
        Set<ConstraintViolation<UserRegistrationResource>> violations = validator.validate(userRegistrationResource);
        boolean result = true;

        List<String> errors = getErrorsMessagesFromViolations(violations);
        if (errors.isEmpty()) {
            List<String> registerErrors = userService.register(userRegistrationResource);
            errors.addAll(registerErrors);
        }

        if (!errors.isEmpty()) {
            result = false;
        }

        return new ResponseResource(result, null, errors);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseResource login(@RequestBody UserLoginRequestResource userLoginRequestResource) {
        HashMap<Object, Object> content = new HashMap<>();

        LoginResultResource loginResult = userService.login(userLoginRequestResource);
        content.put("authToken", loginResult.getToken());

        return new ResponseResource(loginResult.getResult(), content, loginResult.getErrors());
    }

    private List<String> getErrorsMessagesFromViolations(Set<ConstraintViolation<UserRegistrationResource>> violations) {
        List<String> errors = new ArrayList<>();

        for (ConstraintViolation<UserRegistrationResource> violation : violations) {
            errors.add(violation.getMessage());
        }

        return errors;
    }
}
