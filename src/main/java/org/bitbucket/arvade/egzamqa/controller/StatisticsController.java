package org.bitbucket.arvade.egzamqa.controller;

import org.bitbucket.arvade.egzamqa.resource.RequestStatisticsResource;
import org.bitbucket.arvade.egzamqa.resource.ResponseResource;
import org.bitbucket.arvade.egzamqa.service.SingleQuestionEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StatisticsController {

    private SingleQuestionEntryService singleQuestionEntryService;

    @Autowired
    public StatisticsController(SingleQuestionEntryService singleQuestionEntryService) {
        this.singleQuestionEntryService = singleQuestionEntryService;
    }

    @PostMapping(value = "/statistics/:exactDate")
    public ResponseResource getAnsweredQuestionsForExactDate(@RequestHeader(value = "Authorization") String authToken,
                                                             @RequestBody RequestStatisticsResource statisticsResource) {
        ResponseResource response = new ResponseResource();


        return response;
    }
}
