package org.bitbucket.arvade.egzamqa.controller;

import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.resource.CheckAnswerResource;
import org.bitbucket.arvade.egzamqa.resource.QuestionResource;
import org.bitbucket.arvade.egzamqa.resource.ResponseResource;
import org.bitbucket.arvade.egzamqa.resource.SingleQuestionResource;
import org.bitbucket.arvade.egzamqa.service.*;
import org.bitbucket.arvade.egzamqa.validator.AuthTokenValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class QuestionController {


    private QuestionService questionService;
    private PermissionService permissionService;
    private SessionService sessionService;
    private UserService userService;
    private SingleQuestionEntryService singleQuestionEntryService;
    private AuthTokenValidator authTokenValidator;

    @Autowired
    public QuestionController(QuestionService questionService,
                              PermissionService permissionService,
                              SessionService sessionService,
                              UserService userService, SingleQuestionEntryService singleQuestionEntryService,
                              AuthTokenValidator authTokenValidator) {
        this.questionService = questionService;
        this.permissionService = permissionService;
        this.sessionService = sessionService;
        this.userService = userService;
        this.singleQuestionEntryService = singleQuestionEntryService;
        this.authTokenValidator = authTokenValidator;
    }

    @CrossOrigin
    @GetMapping(path = "/questions")
    public ResponseResource getQuestions(@RequestHeader("Authorization") String authToken) {
        ResponseResource response = new ResponseResource();

        if (authToken.isEmpty() || authToken.equals("null")) {
            response.setSuccess(false);
            response.setHttpStatus(HttpStatus.UNAUTHORIZED);
            return response;
        }

        validatePermissionAndSession(response, "download_all_question", authToken);

        if (response.getSuccess()) {
            List<QuestionResource> allQuestions = questionService.getAll();
            response.addResponseObject("all_questions", allQuestions);
        }

        return response;
    }


    @CrossOrigin
    @GetMapping(path = "/singleQuestion")
    public ResponseResource getSingleQuestion(@RequestHeader(value = "Authorization", required = false) String authToken) {
        ResponseResource response = new ResponseResource();

        if (!authTokenValidator.validate(authToken)) {
            response.setSuccess(false);
            response.setHttpStatus(HttpStatus.UNAUTHORIZED);
            return response;
        }

        User user = userService.getUserForAuthToken(authToken);
        validatePermissionAndSession(response, "download_single_question", authToken, user);

        if (response.getSuccess()) {
            SingleQuestionResource singleQuestion = questionService.getSingleRandomQuestion(user);
            response.addResponseObject("singleQuestion", singleQuestion);
        }

        return response;
    }

    @CrossOrigin
    @PostMapping(path = "/checkAnswer")
    public ResponseResource checkAnswer(@RequestHeader(value = "Authorization", required = false) String authToken,
                                        @RequestBody CheckAnswerResource checkAnswerResource) {
        ResponseResource response = new ResponseResource();

        if (!authTokenValidator.validate(authToken)) {
            response.setSuccess(false);
            response.setHttpStatus(HttpStatus.UNAUTHORIZED);
            return response;
        }

        User user = userService.getUserForAuthToken(authToken);
        validatePermissionAndSession(response, "checkAnswer", authToken, user);

        if (!response.getSuccess()) {
            return response;
        }

        Long questionId = checkAnswerResource.getQuestionId();
        Integer answerIndex = checkAnswerResource.getAnswerIndex();

        Integer correctAnswerIndex = questionService.getCorrectAnswerIndex(questionId);

        response.addResponseObject("correctAnswerIndex", correctAnswerIndex);
        Boolean isCorrectAnswer = correctAnswerIndex.equals(answerIndex);

        singleQuestionEntryService.answerOnDownloadedQuestion(questionId, user, isCorrectAnswer);

        return response;
    }

    private void validatePermissionAndSession(ResponseResource response, String actionName, String authToken) {
        User user = userService.getUserForAuthToken(authToken);
        validatePermissionAndSession(response, actionName, authToken, user);
    }

    private void validatePermissionAndSession(ResponseResource response, String actionName, String authToken, User user) {
        if (!permissionService.hasPermission(actionName, user)) {
            response.addError("You don't have permission to do that");
        }

        if (!sessionService.isActive(authToken)) {
            response.setHttpStatus(HttpStatus.UNAUTHORIZED);
            response.addError("Your session expired. Please log in.");
        }
    }
}
