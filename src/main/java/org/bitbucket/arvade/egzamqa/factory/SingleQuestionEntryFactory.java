package org.bitbucket.arvade.egzamqa.factory;

import org.bitbucket.arvade.egzamqa.model.Question;
import org.bitbucket.arvade.egzamqa.model.SingleQuestionEntry;
import org.bitbucket.arvade.egzamqa.model.User;

public interface SingleQuestionEntryFactory {

    SingleQuestionEntry createSingleQuestionEntry(Question question, User user);
}
