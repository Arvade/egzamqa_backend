package org.bitbucket.arvade.egzamqa.factory.impl;

import org.apache.commons.lang3.time.DateUtils;
import org.bitbucket.arvade.egzamqa.config.UserSessionConfig;
import org.bitbucket.arvade.egzamqa.factory.UserSessionFactory;
import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.model.UserSession;
import org.bitbucket.arvade.egzamqa.provider.UserSessionConfigProvider;
import org.bitbucket.arvade.egzamqa.utils.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class UserSessionFactoryImpl implements UserSessionFactory {

    private UserSessionConfigProvider userSessionConfigProvider;
    private RandomStringGenerator randomStringGenerator;

    @Autowired
    public UserSessionFactoryImpl(UserSessionConfigProvider userSessionConfigProvider,
                                  RandomStringGenerator randomStringGenerator) {
        this.userSessionConfigProvider = userSessionConfigProvider;
        this.randomStringGenerator = randomStringGenerator;
    }


    @Override
    public UserSession createSession(User user) {
        UserSession userSession = new UserSession();
        UserSessionConfig userSessionConfig = userSessionConfigProvider.getDefault();

        String secret = randomStringGenerator.nextString();

        Date expirationDate = DateUtils.addHours(new Date(), Math.toIntExact(userSessionConfig.getSessionDuration().toHours()));
        userSession.setExpirationDate(expirationDate);
        userSession.setSecret(secret);
        userSession.setUser(user);

        return userSession;
    }
}
