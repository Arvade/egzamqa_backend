package org.bitbucket.arvade.egzamqa.factory.impl;

import org.bitbucket.arvade.egzamqa.factory.SingleQuestionEntryFactory;
import org.bitbucket.arvade.egzamqa.model.Question;
import org.bitbucket.arvade.egzamqa.model.SingleQuestionEntry;
import org.bitbucket.arvade.egzamqa.model.User;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class SingleQuestionEntryFactoryImpl implements SingleQuestionEntryFactory {

    @Override
    public SingleQuestionEntry createSingleQuestionEntry(Question question, User user) {
        SingleQuestionEntry singleQuestionEntry = new SingleQuestionEntry();

        singleQuestionEntry.setRequestDate(new Date());
        singleQuestionEntry.setUser(user);
        singleQuestionEntry.setQuestion(question);

        return singleQuestionEntry;
    }
}
