package org.bitbucket.arvade.egzamqa.factory;

import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.model.UserSession;

public interface UserSessionFactory {

    UserSession createSession(User user);
}
