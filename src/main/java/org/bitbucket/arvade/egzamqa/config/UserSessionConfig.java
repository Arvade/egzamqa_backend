package org.bitbucket.arvade.egzamqa.config;

import lombok.Builder;
import lombok.Getter;

import java.time.Duration;

@Builder
@Getter
public class UserSessionConfig {

    private Duration sessionDuration;
}
