package org.bitbucket.arvade.egzamqa.resource;

import lombok.Data;
import org.bitbucket.arvade.egzamqa.annotation.PasswordMatches;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@PasswordMatches
public class UserRegistrationResource {

    @NotNull(message = "Username is missing")
    @Size(min = 3, max = 10, message = "Incorrect username length")
    private String username;

    @NotNull(message = "Password is missing")
    @Size(min = 8, max = 30, message = "Incorrect password length")
    @Pattern(regexp = "^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\\d]){1,})(?=(.*[\\W]){1,})(?!.*\\s).{8,}$",
            message = "Password is not strong enough")
    private String password;

    @NotNull(message = "Password confirmation is missing")
    @Size(min = 8, max = 30)
    private String passwordConfirm;

    @NotNull(message = "Email is missing")
    @Email(message = "Incorrect email")
    private String email;
}