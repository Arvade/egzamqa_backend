package org.bitbucket.arvade.egzamqa.resource;

import lombok.Data;

import java.util.Date;

@Data
public class RequestStatisticsResource {

    private Date date;
}
