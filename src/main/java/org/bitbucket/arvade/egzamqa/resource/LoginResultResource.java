package org.bitbucket.arvade.egzamqa.resource;

import lombok.Data;

import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class LoginResultResource {

    private Boolean result = true;

    private List<String> errors = new ArrayList<>();

    private String token;

    public void addError(String error) {
        this.errors.add(error);
    }
}
