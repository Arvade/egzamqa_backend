package org.bitbucket.arvade.egzamqa.resource;

import lombok.Data;

@Data
public class AnswerResource {

    private String content;

    private Boolean isCorrect;
}
