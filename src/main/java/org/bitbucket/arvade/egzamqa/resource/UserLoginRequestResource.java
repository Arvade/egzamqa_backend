package org.bitbucket.arvade.egzamqa.resource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginRequestResource {

    private String username;

    private String password;
}
