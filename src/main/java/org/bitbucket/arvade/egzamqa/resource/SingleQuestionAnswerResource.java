package org.bitbucket.arvade.egzamqa.resource;

import lombok.Data;

@Data
public class SingleQuestionAnswerResource {

    private String content;
}
