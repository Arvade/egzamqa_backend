package org.bitbucket.arvade.egzamqa.resource;

import lombok.Data;

@Data
public class CheckAnswerResource {

    private Long questionId;

    private Integer answerIndex;
}
