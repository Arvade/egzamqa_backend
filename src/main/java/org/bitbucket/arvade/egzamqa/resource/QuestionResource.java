package org.bitbucket.arvade.egzamqa.resource;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class QuestionResource {

    private Long id;

    private String content;

    private List<AnswerResource> answers = new ArrayList<>();
}
