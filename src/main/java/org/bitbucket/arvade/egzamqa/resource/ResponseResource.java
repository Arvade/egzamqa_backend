package org.bitbucket.arvade.egzamqa.resource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseResource {

    private Boolean success = true;

    private Map<Object, Object> content = new HashMap<>();

    private List<String> errors = new ArrayList<>();

    private HttpStatus httpStatus;

    public ResponseResource(Boolean success, Map<Object, Object> content, List<String> errors) {
        this.success = success;
        this.content = content;
        this.errors = errors;
    }

    public void addError(String errorMessage) {
        errors.add(errorMessage);
        success = false;
    }

    public void addAll(List<String> errorMessages) {
        if (!errorMessages.isEmpty()) {
            errors.addAll(errorMessages);
            success = false;
        }
    }

    public void addResponseObject(Object key, Object value) {
        content.put(key, value);
    }

    public void addAll(Map<Object, Object> content){
        this.content.putAll(content);
    }
}
