package org.bitbucket.arvade.egzamqa.factory;

import org.bitbucket.arvade.egzamqa.config.UserSessionConfig;
import org.bitbucket.arvade.egzamqa.factory.impl.UserSessionFactoryImpl;
import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.model.UserSession;
import org.bitbucket.arvade.egzamqa.provider.UserSessionConfigProvider;
import org.bitbucket.arvade.egzamqa.utils.RandomStringGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.time.Duration;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserSessionFactoryImplTest {

    @Mock
    private UserSessionConfigProvider userSessionConfigProvider;

    @Mock
    private RandomStringGenerator randomStringGenerator;

    @InjectMocks
    private UserSessionFactoryImpl factory;

    @Before
    public void setUp() {
        when(randomStringGenerator.nextString())
                .thenReturn("random-string");

        when(userSessionConfigProvider.getDefault())
                .thenAnswer((Answer<UserSessionConfig>) invocation -> UserSessionConfig.builder()
                        .sessionDuration(Duration.ofHours(2))
                        .build());
    }

    @Test
    public void shouldReturnUserSessionForUser() {
        // Given
        User user = new User();

        UserSession expectedUserSession = new UserSession();
        expectedUserSession.setUser(user);
        expectedUserSession.setSecret(randomStringGenerator.nextString());

        // When
        UserSession result = factory.createSession(user);

        // Then
        assertThat(result).isEqualToIgnoringGivenFields(expectedUserSession, "expirationDate");
    }
}
