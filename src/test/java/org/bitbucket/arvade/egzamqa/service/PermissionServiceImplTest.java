package org.bitbucket.arvade.egzamqa.service;

import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.provider.PermissionsProvider;
import org.bitbucket.arvade.egzamqa.service.impl.PermissionServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PermissionServiceImplTest {

    @Mock
    private UserService userService;

    @Mock
    private PermissionsProvider permissionsProvider;

    @InjectMocks
    private PermissionServiceImpl permissionService;

    @Before
    public void setUp() {
        HashMap<String, HashMap<String, Boolean>> permissions = new HashMap<>();
        HashMap<String, Boolean> permissionsForUser = new HashMap<>();
        permissionsForUser.put("download", true);
        permissionsForUser.put("update", false);

        HashMap<String, Boolean> permissionForModerator = new HashMap<>();
        permissionForModerator.put("download", true);
        permissionForModerator.put("update", true);


        permissions.put("user", permissionsForUser);
        permissions.put("moderator", permissionForModerator);

        when(permissionsProvider.getPermissions())
                .thenReturn(permissions);
    }

    @Test
    public void shouldReturnTrueWhenUserHasPermission() {
        // Given
        String actionName = "download";
        User user = new User();
        user.setRole("user");

        // When
        Boolean result = permissionService.hasPermission(actionName, user);

        // Then
        assertThat(result).isEqualTo(true);
    }

    @Test
    public void shouldReturnFalseWhenUserHasNotPermission() {
        // Given
        String actionName = "update";
        User user = new User();
        user.setRole("user");

        // When
        Boolean result = permissionService.hasPermission(actionName, user);

        // Then
        assertThat(result).isEqualTo(false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowAnExceptionWhenUnknownActionProvided() {
        // Given
        String actionName = "makecookies";
        User user = new User();
        user.setRole("user");

        // Then
        permissionService.hasPermission(actionName, user);
    }
}
