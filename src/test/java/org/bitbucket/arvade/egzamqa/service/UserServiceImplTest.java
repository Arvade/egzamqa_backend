package org.bitbucket.arvade.egzamqa.service;

import org.bitbucket.arvade.egzamqa.factory.UserSessionFactory;
import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.model.UserSession;
import org.bitbucket.arvade.egzamqa.repository.UserRepository;
import org.bitbucket.arvade.egzamqa.resource.LoginResultResource;
import org.bitbucket.arvade.egzamqa.resource.UserLoginRequestResource;
import org.bitbucket.arvade.egzamqa.resource.UserRegistrationResource;
import org.bitbucket.arvade.egzamqa.service.impl.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.runners.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {


    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private SessionService sessionService;
    private UserSessionFactory userSessionFactory;
    private UserRepository userRepository;
    private ModelMapper modelMapper;

    private UserServiceImpl userService;

    @Before
    public void setUp() {
        bCryptPasswordEncoder = mock(BCryptPasswordEncoder.class);
        userSessionFactory = mock(UserSessionFactory.class);
        userRepository = mock(UserRepository.class);
        modelMapper = mock(ModelMapper.class);
        sessionService = mock(SessionService.class);

        userService = new UserServiceImpl(userRepository,
                bCryptPasswordEncoder,
                userSessionFactory,
                modelMapper,
                sessionService);

        when(userRepository.findByUsername("arvade"))
                .thenReturn(new User(10L, "arvade", "password", "arvade@domain.com", null, null));

        when(userRepository.findByUsername("feaq16"))
                .thenReturn(null);

        when(userRepository.findByEmail("arvade@domain.com"))
                .thenReturn(new User(10L, "arvade", "password", "arvade@domain.com", null, null));

        when(userRepository.findByEmail("feaq16@domain.com"))
                .thenReturn(null);
    }

    @Test
    public void shouldReturnErrorMessageWhenUsernameOrPasswordIsNull() {
        // Given
        UserLoginRequestResource loginRequestResource = new UserLoginRequestResource();
        loginRequestResource.setUsername(null);
        loginRequestResource.setPassword("password");

        String expectedErrorMessage = "Invalid username and password";

        // When
        LoginResultResource result = userService.login(loginRequestResource);

        // Then
        assertThat(result.getResult()).isEqualTo(false);
        assertThat(result.getErrors().get(0)).isEqualTo(expectedErrorMessage);
        assertThat(result.getToken()).isEqualTo(null);
    }

    @Test
    public void shouldReturnErrorMessageWhenUsernameDoesntExists() {
        // Given
        String username = "feaq16";
        String password = "password";
        UserLoginRequestResource loginRequestResource = new UserLoginRequestResource(username, password);


        List<String> expectedErrors = new ArrayList<>();
        expectedErrors.add("Invalid username and password");

        LoginResultResource expectedLoginResult = new LoginResultResource();
        expectedLoginResult.setResult(false);
        expectedLoginResult.setErrors(expectedErrors);
        expectedLoginResult.setToken(null);

        // When
        LoginResultResource result = userService.login(loginRequestResource);

        // Then
        assertThat(result).isEqualTo(expectedLoginResult);
    }

    @Test
    public void shouldReturnErrorMessageWhenPasswordsDoesntMatch() {
        // Given
        String username = "arvade";
        String password = "password";
        UserLoginRequestResource loginRequestResource = new UserLoginRequestResource(username, password);

        when(bCryptPasswordEncoder.matches(Matchers.eq(password), anyString()))
                .thenReturn(false);


        List<String> expectedErrors = new ArrayList<>();
        expectedErrors.add("Invalid username and password");

        LoginResultResource expectedLoginResult = new LoginResultResource();
        expectedLoginResult.setToken(null);
        expectedLoginResult.setResult(false);

        expectedLoginResult.setErrors(expectedErrors);


        // When
        LoginResultResource result = userService.login(loginRequestResource);

        // Then
        assertThat(result).isEqualTo(result);
    }

    @Test
    public void shouldReturnSuccessResultWithTokenWhen() {
        // Given
        String username = "arvade";
        String password = "password";
        String expectedToken = "my-new-super-legit-auth-secret";
        UserLoginRequestResource loginRequestResource = new UserLoginRequestResource(username, password);

        when(bCryptPasswordEncoder.matches(anyString(), anyString()))
                .thenReturn(true);
        when(sessionService.getAuthToken(any(UserSession.class)))
                .thenReturn(expectedToken);


        LoginResultResource expectedLoginResult = new LoginResultResource();
        expectedLoginResult.setToken(expectedToken);
        expectedLoginResult.setResult(true);
        expectedLoginResult.setErrors(new ArrayList<>());

        // When
        LoginResultResource result = userService.login(loginRequestResource);

        // Then
        assertThat(result).isEqualTo(expectedLoginResult);
    }

    @Test
    public void shouldReturnErrorThatUsernameAlreadyTaken() {
        // Given
        UserRegistrationResource registrationResource = new UserRegistrationResource();
        registrationResource.setUsername("arvade");
        registrationResource.setEmail("feaq16@domain.com");

        List<String> expectedErrorMessages = new ArrayList<>();
        expectedErrorMessages.add("Username already taken");

        // When
        List<String> result = userService.register(registrationResource);

        // Then
        assertThat(result).containsAll(expectedErrorMessages);
    }

    @Test
    public void shouldReturnErrorThatEmailIsAlreadyTaken() {
        // Given
        UserRegistrationResource registrationResource = new UserRegistrationResource();
        registrationResource.setEmail("feaq16");
        registrationResource.setEmail("arvade@domain.com");

        List<String> expectedErrorMessages = new ArrayList<>();
        expectedErrorMessages.add("Email already taken");

        // When
        List<String> result = userService.register(registrationResource);

        // Then
        assertThat(result).containsAll(expectedErrorMessages);
    }

    @Test
    public void shouldSaveProvidedCredentialsAsNewUser() {
        // Given
        String username = "feaq16";
        String email = "feaq16@domain.com";
        String password = "password";
        String passwordConfirm = password;

        UserRegistrationResource registrationResource = new UserRegistrationResource();
        registrationResource.setUsername(username);
        registrationResource.setEmail(email);
        registrationResource.setPassword(password);
        registrationResource.setPasswordConfirm(passwordConfirm);


        when(modelMapper.map(registrationResource, User.class))
                .thenReturn(new User(null, username, password, email, new Date(), null));
        when(bCryptPasswordEncoder.encode(anyString())).thenCallRealMethod();

        // When
        List<String> result = userService.register(registrationResource);

        // Then
        assertThat(result).isEmpty();
        verify(userRepository, times(1)).save(Matchers.any(User.class));
    }
}