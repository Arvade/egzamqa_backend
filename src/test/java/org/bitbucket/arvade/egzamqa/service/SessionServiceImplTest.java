package org.bitbucket.arvade.egzamqa.service;


import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.model.UserSession;
import org.bitbucket.arvade.egzamqa.repository.UserSessionRepository;
import org.bitbucket.arvade.egzamqa.service.impl.SessionServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Base64;
import java.util.Date;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class SessionServiceImplTest {

    private UserSessionRepository userSessionRepository;
    private Base64.Encoder encoder;
    private Base64.Decoder decoder;

    private SessionService sessionService;

    @Before
    public void setUp() {
        userSessionRepository = mock(UserSessionRepository.class);
        encoder = Base64.getEncoder();
        decoder = Base64.getDecoder();

        sessionService = new SessionServiceImpl(userSessionRepository, encoder, decoder);
    }

    @Test
    public void shouldReturnFalseWhenGivenTokenIsNotActive() {
        // Given
        String token = "MTM6Y29vbFNlY3JldC47Ly5bd293";

        String[] parts = sessionService.decodeAuthTokenToTokenParts(token);
        Long userId = Long.parseLong(parts[0]);
        String secret = parts[1];
        Date now = new Date();

        when(userSessionRepository.findTop1BySecretAndUserAndExpirationDateGreaterThan(secret, new User(userId), now))
                .thenReturn(null);

        // When
        Boolean result = sessionService.isActive(token);

        // Then
        assertThat(result).isEqualTo(false);
    }

    @Test
    public void shouldReturnPartsOfToken() {
        // Given
        String token = "MTM6Y29vbFNlY3JldC47Ly5bd293";
        Long expectedUserId = 13L;
        String expectedSecret = "coolSecret.;/.[wow";

        // When
        String[] parts = sessionService.decodeAuthTokenToTokenParts(token);

        // Then
        Long userId = Long.parseLong(parts[0]);
        String secret = parts[1];

        assertThat(userId).isEqualTo(expectedUserId);
        assertThat(secret).isEqualTo(expectedSecret);
    }

    @Test
    public void shouldReturnStringToken() {
        // Given
        UserSession userSession = new UserSession(null, "coolSecret.;/.[wow", new User(13L), new Date());
        String expected = "MTM6Y29vbFNlY3JldC47Ly5bd293";

        // When
        String result = sessionService.getAuthToken(userSession);

        // Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldInvokeSaveOnRepository() {
        // Given
        UserSession userSession = new UserSession();

        // When
        sessionService.save(userSession);

        // Then
        verify(userSessionRepository, times(1)).save(userSession);
    }
}