package org.bitbucket.arvade.egzamqa.service;

import org.bitbucket.arvade.egzamqa.model.Answer;
import org.bitbucket.arvade.egzamqa.model.Question;
import org.bitbucket.arvade.egzamqa.model.User;
import org.bitbucket.arvade.egzamqa.repository.QuestionRepository;
import org.bitbucket.arvade.egzamqa.resource.QuestionResource;
import org.bitbucket.arvade.egzamqa.resource.SingleQuestionResource;
import org.bitbucket.arvade.egzamqa.service.impl.QuestionServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class QuestionServiceImplTest {

    @Mock
    private SingleQuestionEntryService singleQuestionEntryService;

    @Mock
    private QuestionRepository questionRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private QuestionServiceImpl service;

    private List<Question> questionList = new ArrayList<>();
    private List<QuestionResource> questionResourceList = new ArrayList<>();


    @Before
    public void setUp() {
        questionList.add(new Question());
        questionList.add(new Question());
        questionList.add(new Question());

        questionResourceList.add(new QuestionResource());
        questionResourceList.add(new QuestionResource());
        questionResourceList.add(new QuestionResource());

        when(questionRepository.findAll())
                .thenReturn(questionList);

    }

    @Test
    public void shouldReturnAllQuestions() {
        // When
        List<QuestionResource> result = service.getAll();

        // Then
        assertThat(result).asList().hasSize(questionList.size());
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenNoRandomQuestionFound() {
        // Given
        User user = new User("username", "password", "user@domain.com", new Date(), "user");
        when(questionRepository.findRandom())
                .thenReturn(null);

        // When
        service.getSingleRandomQuestion(user);
    }


    @Test
    public void shouldReturnSingleQuestion() {
        // Given
        User user = new User("username", "password", "user@domain.com", new Date(), "user");

        SingleQuestionResource expected = new SingleQuestionResource();

        Question question = new Question();
        when(questionRepository.findRandom())
                .thenReturn(question);

        when(modelMapper.map(question, SingleQuestionResource.class))
                .thenReturn(expected);

        // When
        SingleQuestionResource result = service.getSingleRandomQuestion(user);

        // Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldReturnCorrectAnswerIndexForQuestionId() {
        // Given
        List<Answer> answerList = new ArrayList<>();
        answerList.add(new Answer("No", false));
        answerList.add(new Answer("Yes Yes", true));
        answerList.add(new Answer("Yes", false));
        answerList.add(new Answer("Yes no", false));

        Question question = new Question("May the Force be with you?", answerList);
        Long questionId = 237L;
        when(questionRepository.findOne(questionId))
                .thenReturn(question);

        Integer expectedCorrectAnswerIndex = 1;

        // When
        Integer result = service.getCorrectAnswerIndex(questionId);

        // Then
        assertThat(result).isEqualTo(expectedCorrectAnswerIndex);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenNoQuestionFoundForProvidedId() {
        // Given
        Long questionId = 42L;
        when(questionRepository.findOne(questionId))
                .thenReturn(null);

        // When
        service.getCorrectAnswerIndex(questionId);
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowExceptionWhenNoCorrectAnswerFound() {
        // Given

        List<Answer> answerList = new ArrayList<>();
        answerList.add(new Answer("No", false));
        answerList.add(new Answer("Yes Yes", false));
        answerList.add(new Answer("Yes", false));
        answerList.add(new Answer("Yes no", false));

        Question question = new Question("May the Force be with you?", answerList);
        Long questionId = 237L;
        when(questionRepository.findOne(questionId))
                .thenReturn(question);

        // When
        service.getCorrectAnswerIndex(questionId);
    }

    @Test
    public void shouldReturnCorrectAnswerIndexWhenListOfAnswerProvided() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // Given
        Method method = service.getClass().getDeclaredMethod("getCorrectAnswerIndex", List.class);
        method.setAccessible(true);

        List<Answer> answerList = new ArrayList<>();
        answerList.add(new Answer("a", false));
        answerList.add(new Answer("b", false));
        answerList.add(new Answer("c", true));
        answerList.add(new Answer("d", false));

        Integer expected = 2;


        // When
        Integer result = (Integer) method.invoke(service, answerList);

        // Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldReturnNegativeValueWhenCorrectAnswerFound() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        Method method = service.getClass().getDeclaredMethod("getCorrectAnswerIndex", List.class);
        method.setAccessible(true);

        List<Answer> answerList = new ArrayList<>();
        answerList.add(new Answer("a", false));
        answerList.add(new Answer("b", false));
        answerList.add(new Answer("c", false));
        answerList.add(new Answer("d", false));

        Integer expected = -1;

        // When
        Integer result = (Integer) method.invoke(service, answerList);

        // Then
        assertThat(result).isEqualTo(expected);
    }
}
