package org.bitbucket.arvade.egzamqa.provider;

import org.bitbucket.arvade.egzamqa.config.UserSessionConfig;
import org.bitbucket.arvade.egzamqa.provider.impl.UserSessionConfigProviderImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class UserSessionConfigProviderImplTest {

    private UserSessionConfigProviderImpl provider = new UserSessionConfigProviderImpl();

    @Test
    public void shouldReturnUserSessionConfig() {
        // When
        UserSessionConfig aDefault = provider.getDefault();

        // Then
        assertThat(aDefault).isNotNull();
    }
}
