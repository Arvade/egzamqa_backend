package org.bitbucket.arvade.egzamqa.provider;

import org.bitbucket.arvade.egzamqa.provider.impl.PermissionsProviderImpl;
import org.json.simple.parser.JSONParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PermissionsProviderImplTest {

    @Mock
    private JSONParser jsonParser;

    @InjectMocks
    private PermissionsProviderImpl provider;

    @Test(expected = IllegalStateException.class)
    public void shouldThrowAnExceptionWhenPermissionFileDoesntExists() {
        // Given
        String permissionsFileNameThatDoesntExists = "idontexists";

        provider.setDefaultPermissionFileName(permissionsFileNameThatDoesntExists);

        // When
        provider.getPermissions();
    }
}