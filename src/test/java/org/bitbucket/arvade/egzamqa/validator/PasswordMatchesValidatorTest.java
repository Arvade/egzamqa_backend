package org.bitbucket.arvade.egzamqa.validator;

import org.bitbucket.arvade.egzamqa.resource.UserRegistrationResource;
import org.bitbucket.arvade.egzamqa.validator.impl.PasswordMatchesValidator;
import org.junit.Test;
import org.mockito.Mock;

import javax.validation.ConstraintValidatorContext;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class PasswordMatchesValidatorTest {

    private PasswordMatchesValidator validator = new PasswordMatchesValidator();

    @Mock
    private ConstraintValidatorContext constraintValidatorContext;

    @Test
    public void shouldReturnFalseWhenProvidedPasswordIsNotValid() {
        // Given
        UserRegistrationResource registrationResource = new UserRegistrationResource();
        registrationResource.setPassword("password");
        registrationResource.setPassword("passwordConfirm");


        // When
        boolean result = validator.isValid(registrationResource, constraintValidatorContext);

        // Then
        assertThat(result).isEqualTo(false);
    }

    @Test
    public void shouldReturnTrueWhenProvidedPasswordIsValid() {
        // Given
        UserRegistrationResource registrationResource = new UserRegistrationResource();
        registrationResource.setPassword("password");
        registrationResource.setPasswordConfirm("password");

        // When
        boolean result = validator.isValid(registrationResource, constraintValidatorContext);

        // Then
        assertThat(result).isEqualTo(true);
    }
}
